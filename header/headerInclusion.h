//
// Created by Theo on 25/09/2018.
//

#ifndef MARIOSOKOBAN_HEADERINCLUSION_H
#define MARIOSOKOBAN_HEADERINCLUSION_H

        #include <stdlib.h>
        #include <stdio.h>

        #include "../SDL2/include/SDL.h"
        #include "../SDL2/include/SDL_image.h"

        #include "../header/constantes.h"
        #include "../header/chargerNiveau.h"
        #include "../header/jeu.h"
        #include "deplacerJoueurOuCaisse.h"

#endif //MARIOSOKOBAN_HEADERINCLUSION_H
