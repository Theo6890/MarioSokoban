//
// Created by Theo on 14/03/2018.
//

#ifndef MARIOSOKOBAN_DEPLACERJOUEUR_H
#define MARIOSOKOBAN_DEPLACERJOUEUR_H

#include "../header/constantes.h"
#include "../SDL2/include/SDL.h"


    void deplacerJoueurOuCaisse(int carte[TAILLE_BLOCS][TAILLE_BLOCS], SDL_Rect *pos, SDL_Keycode toucheClavier,
                                SDL_Rect *positionCaisse, int *objectifRestants);

#endif //MARIOSOKOBAN_DEPLACERJOUEUR_H
