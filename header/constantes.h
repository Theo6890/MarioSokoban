/*
constantes.h
-------------

Rôle : définit les constantes qui seront utilisées dans tout le programme (taille, fenêtre, taille bloc, nombre de blocs...)
*/

#ifndef DEF_CONSTANTES
#define DEF_CONSTANTES

    #define TAILLE_BLOCS        34 // Taille d'un bloc en pixels
    #define NB_BLOCS_LARGEUR    12
    #define NB_BLOCS_HAUTEUR    12
    #define LARGEUR_FENETRE     NB_BLOCS_LARGEUR * TAILLE_BLOCS
    #define HAUTEUR_FENETRE     NB_BLOCS_HAUTEUR * TAILLE_BLOCS
    #define true 1
    #define false 0

    enum {VIDE, MUR, CAISSE, OBJECTIF, CAISSE_SUR_OBJECTIF, MARIO, CAISSE_BLOQUEE};
    typedef int bool;
#endif // DEF_CONSTANTES
