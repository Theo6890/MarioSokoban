/*
fichier.h
----------

R�le : D�finit les protoypes des fonctions utilis�es pour charger un niveau (depuis un fichier ou non).
*/
#ifndef DEF_FICHIER
#define DEF_FICHIER
    #include "../header/constantes.h"
    int chargerNiveau(int carte[TAILLE_BLOCS][TAILLE_BLOCS]);
#endif // DEF_FICHIER
