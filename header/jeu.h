/*
jeu.h
------

R�le : d�finit les prototypes des fonctions d�di�s � une partie du jeu.
*/
#ifndef DEF_JEU
#define DEF_JEU
    #include "../header/constantes.h"
    #include "../SDL2/include/SDL.h"

    void jouer(SDL_Window *window, SDL_Renderer *renderer);
    SDL_Texture* afficherMario(SDL_Renderer *renderer, SDL_Keycode toucheClavier);

#endif // DEF_JEU
