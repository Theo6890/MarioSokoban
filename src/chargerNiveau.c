/*
fichier.c
----------

Rôle : Créer les fonctions pour charger un niveau (depuis un fichier ou non).
*/


#include "../header/headerInclusion.h"




int chargerNiveau(int carte[TAILLE_BLOCS][TAILLE_BLOCS])
{
    FILE *level = NULL;
    int caractereActuel = 0, i = 0, j= 0;


    if ( (level = fopen("../level/level1.txt", "r")) == NULL) {
        perror("../level/level1.txt");
        exit(EXIT_FAILURE);
    }
    else {
            // Boucle de lecture des caractères un à un
            for (i = 0; i <= NB_BLOCS_HAUTEUR; i++) {
                for (j = 0; j <= NB_BLOCS_LARGEUR; j++) {
                    caractereActuel = fgetc(level); // On lit le caractère
                    if (caractereActuel != '\n' && caractereActuel != 0) {

                        if (caractereActuel == '|') {
                            carte[i][j] = MUR;
                        } else if (caractereActuel == 'C') {
                            carte[i][j] = CAISSE;
                        } else if (caractereActuel == 'O') {
                            carte[i][j] = OBJECTIF;
                        } else if (caractereActuel == 'M') {
                            carte[i][j] = MARIO;
                        }
                    }
                }
            }
    }

    fclose(level);


    return EXIT_SUCCESS;
}
