/*
jeu.c
------

Rôle : corps du jeu. Permet de lancer une nouvelle partie du jeu.
*/


#include "../header/headerInclusion.h"


void jouer(SDL_Window *window, SDL_Renderer *renderer) {

    // Couleur de fond blanc
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
    // On efface l'écran avant d'afficher autre chose
    SDL_RenderClear(renderer);




/*------------------------------------ Initialisation des varaibles et instanciation ---------------------------------*/
//    Déclaration des variables
    SDL_Surface *imageMur = NULL, *imageCaisse = NULL, *imageCaisseOk = NULL, *imageObjectif = NULL, *imageMarioDefault = NULL,
            *imageGameWin = NULL, *imageGameOver = NULL;
    SDL_Texture *textureMario = NULL, *textureMur = NULL, *textureCaisse = NULL, *textureCaisseOk = NULL,
            *textureObjectif = NULL, *textureGameWin = NULL, *textureGameOver = NULL;
    int *objectifsRestantsPointeur;
    SDL_Rect *positionMarioPointeur = NULL;
    SDL_Rect position, positionMario;
    SDL_Event event;
    int continuer = 1, objectifsRestants = 0, i = 0, j = 0;
    int carte[TAILLE_BLOCS][TAILLE_BLOCS] = {0};

    positionMarioPointeur = &positionMario;
    objectifsRestantsPointeur = &objectifsRestants;

//    Instaciation de la position et de la taille de Mario
    positionMario.h = TAILLE_BLOCS;
    positionMario.w = TAILLE_BLOCS;

//    Instanciation de la taille et la position de tous les éléments
    position.w = TAILLE_BLOCS;
    position.h = TAILLE_BLOCS;


//    Instanciation des surface
    imageMur = IMG_Load("../images/mur.jpg");
    if (!imageMur) {
        printf("IMG_Load: %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }
    imageCaisse = IMG_Load("../images/caisse.jpg");
    if (!imageCaisse) {
        printf("IMG_Load: %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }
    imageCaisseOk = IMG_Load("../images/caisseok.jpg");
    if (!imageCaisseOk) {
        printf("IMG_Load: %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }
    imageObjectif = IMG_Load("../images/objectif.png");
    if (!imageObjectif) {
        printf("IMG_Load: %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }
    imageMarioDefault = IMG_Load("../images/mario_bas.gif");
    if (!imageMarioDefault) {
        printf("IMG_Load: %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }
    imageGameWin = IMG_Load("../images/game_win.jpg");
    if (!imageGameWin) {
        printf("IMG_Load: %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }
    imageGameOver = IMG_Load("../images/game_over.jpg");
    if (!imageGameOver) {
        printf("IMG_Load: %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }

//    Instanciation des textures
    textureMur = SDL_CreateTextureFromSurface(renderer, imageMur);
    if (textureMur == NULL) {
        printf("SDL_Texture: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    textureCaisse = SDL_CreateTextureFromSurface(renderer, imageCaisse);
    if (textureCaisse == NULL) {
        printf("SDL_Texture: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    textureCaisseOk = SDL_CreateTextureFromSurface(renderer, imageCaisseOk);
    if (textureCaisseOk == NULL) {
        printf("SDL_Texture: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    textureObjectif = SDL_CreateTextureFromSurface(renderer, imageObjectif);
    if (textureObjectif == NULL) {
        printf("SDL_Texture: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    textureMario = SDL_CreateTextureFromSurface(renderer, imageMarioDefault);
    if (textureMario == NULL) {
        printf("SDL_Texture: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    textureGameWin = SDL_CreateTextureFromSurface(renderer, imageGameWin);
    if (textureGameWin == NULL) {
        printf("SDL_Texture: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    textureGameOver = SDL_CreateTextureFromSurface(renderer, imageGameOver);
    if (textureGameOver == NULL) {
        printf("SDL_Texture: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    
    
//    On efface toutes les surfaces
    SDL_FreeSurface(imageMur);
    SDL_FreeSurface(imageCaisse);
    SDL_FreeSurface(imageCaisseOk);
    SDL_FreeSurface(imageObjectif);
    SDL_FreeSurface(imageMarioDefault);
    SDL_FreeSurface(imageGameWin);
    SDL_FreeSurface(imageGameOver);

/*----------------------------------------- Fin initialisation des variables -----------------------------------------*/




/*----------------------------------------------- Remplissage de la map ----------------------------------------------*/
//    Tile mapping avec le fichier niveau
    chargerNiveau(carte);

//    On effectue l'affichage du Tile Mapping
    for (i = 0; i < TAILLE_BLOCS; i++) {
        for (j = 0; j < TAILLE_BLOCS; j++) {
            position.x = i * TAILLE_BLOCS;
            position.y = j * TAILLE_BLOCS;

            switch (carte[j][i]) // On inverse i et j pour afficher à l'horizontal
            {
                case MUR:
                    SDL_RenderCopy(renderer, textureMur, NULL, &position);
                    break;
                case CAISSE:
                    SDL_RenderCopy(renderer, textureCaisse, NULL, &position);
                    break;
                case CAISSE_SUR_OBJECTIF:
                    SDL_RenderCopy(renderer, textureCaisseOk, NULL, &position);
                    break;
                case OBJECTIF:
                    SDL_RenderCopy(renderer, textureObjectif, NULL, &position);
                    objectifsRestants++;
                    break;
                case MARIO:
                    positionMario.x = i * TAILLE_BLOCS;
                    positionMario.y = j * TAILLE_BLOCS;
                    break;
                default:
                    break;
            }


        }
    }

/*----------------------------------------------- Fin remplissage map ------------------------------------------------*/




/*-------------------------------------------- Boucle des évènements -------------------------------------------------*/
    while (continuer) {
        SDL_WaitEvent(&event);
        if (*objectifsRestantsPointeur == 0) {
            continuer = 0;
        }
        switch (event.type) {
            case SDL_QUIT: // Appuie sur la croix de la fenêtre
                continuer = 0;
                break;
            case SDL_KEYDOWN: // Appuie sur une touche du clavier

                switch (event.key.keysym.sym) {
                    case SDLK_ESCAPE: // Escape --> quitte le jeu
                        continuer = 0;
                        break;
                    case SDLK_UP: // Flèche du haut --> charge Mario vue de haut
                        textureMario = afficherMario(renderer, SDLK_UP);
                        deplacerJoueurOuCaisse(carte, &positionMario, SDLK_UP, &position, objectifsRestantsPointeur);
                        break;
                    case SDLK_DOWN: // Flèche du bas --> charge Mario vue de bas
                        textureMario = afficherMario(renderer, SDLK_DOWN);
                        deplacerJoueurOuCaisse(carte, &positionMario, SDLK_DOWN, &position, objectifsRestantsPointeur);
                        break;
                    case SDLK_LEFT: // Flèche de gauche --> charge Mario vue de gauche
                        textureMario = afficherMario(renderer, SDLK_LEFT);
                        deplacerJoueurOuCaisse(carte, &positionMario, SDLK_LEFT, &position, objectifsRestantsPointeur);
                        break;
                    case SDLK_RIGHT: // Flèche droite --> charge Mario vue de droite
                        textureMario = afficherMario(renderer, SDLK_RIGHT);
                        deplacerJoueurOuCaisse(carte, &positionMario, SDLK_RIGHT, &position, objectifsRestantsPointeur);
                        break;
                }
                break;
        }

        printf("%d", objectifsRestants);

        //------------------Mise à jour de l'affichage de la fenêtre--------------------

        // On copie la nouvelle image de la imageCaisse dans le rendu
        // On change l'image si la imageCaisse est sur un OBJECTIF ou non
        if (carte[position.y / TAILLE_BLOCS][position.x / TAILLE_BLOCS] == CAISSE_SUR_OBJECTIF) {
            SDL_RenderCopy(renderer, textureCaisseOk, NULL, &position);
        }
        else SDL_RenderCopy(renderer, textureCaisse, NULL, &position);
        // On copie la nouvelle image de Mario dans le rendu
        SDL_RenderCopy(renderer, textureMario, NULL, &positionMario);
        // On met à jour l'affichage de la rectangle
        SDL_RenderPresent(renderer);
        // On met à jour l'image de Mario
        SDL_RenderFillRect(renderer, &positionMario);
        // Si mario se trouve sur un OBJECTIF, on laisse afficher l'imageObjectif sous lui
        if (carte[positionMarioPointeur->y / TAILLE_BLOCS][positionMarioPointeur->x / TAILLE_BLOCS] == OBJECTIF)
        {
            // On réaffiche l'imageObjectif car SDL_RenderFillRect l'a efface
            carte[positionMarioPointeur->y / TAILLE_BLOCS][positionMarioPointeur->x / TAILLE_BLOCS] = OBJECTIF;
            SDL_RenderCopy(renderer, textureObjectif, NULL, &positionMario);
        }
        //---------------Fin :Mise à jour de l'affichage de la fenêtre------------------

    }
/*--------------------------------------------- Fin boucle évènements ------------------------------------------------*/




//    On détruit toutes les textures
    SDL_DestroyTexture(textureMario);
    SDL_DestroyTexture(textureMur);
    SDL_DestroyTexture(textureCaisse);
    SDL_DestroyTexture(textureCaisseOk);
    SDL_DestroyTexture(textureObjectif);
    SDL_DestroyTexture(textureGameWin);
    SDL_DestroyTexture(textureGameOver);
//    On détruit le rendu
    SDL_DestroyRenderer(renderer);
//    On détruit la fenêtre
    SDL_DestroyWindow(window);

    SDL_Quit();
    exit(EXIT_SUCCESS);
}