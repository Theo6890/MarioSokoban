/*
main.c
--------

Rôle : menu du jeu. Permet de choisir entre l'éditeur de jeu et le jeu lui même
*/


#include "../header/headerInclusion.h"




int main (int argc, char *argv[])
{
    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Surface *imageMenu = NULL;
    SDL_Texture *textureMenu = NULL;
    SDL_Rect menu;
    SDL_Event event;
    int continuer = 1;

    // On défini la taille du menu
    menu.x = 0;
    menu.y = 0;
    menu.w = LARGEUR_FENETRE;
    menu.h = HAUTEUR_FENETRE;


    SDL_Init(SDL_INIT_VIDEO);

    /* Initialisation simple */
    if (SDL_Init(SDL_INIT_VIDEO) != 0 )
    {
        fprintf(stdout,"Échec de l'initialisation de la SDL (%s)\n",SDL_GetError());
        exit(EXIT_FAILURE);
    }


    window = SDL_CreateWindow
    (
       "Mario Sokoban",
       SDL_WINDOWPOS_CENTERED,
       SDL_WINDOWPOS_CENTERED,
       LARGEUR_FENETRE,
       HAUTEUR_FENETRE,
       SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
     );

    // Check that the window was successfully created
    if (window == NULL) {
        // In the case that the window could not be made...
        printf("Could not create window: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }


    renderer = SDL_CreateRenderer(window, -1, 0);


    // Instanciation de l'image du menu
    imageMenu = IMG_Load("../images/menu.jpg");
        if(!imageMenu){
            printf("IMG_Load: %s\n", IMG_GetError());
            exit(EXIT_FAILURE);
            }
    // Creation de la texture
    textureMenu = SDL_CreateTextureFromSurface(renderer, imageMenu);
    SDL_FreeSurface(imageMenu);



    while(continuer)
        {
            SDL_WaitEvent(&event);
            switch(event.type)
            {
                case SDL_QUIT: //appuie sur la croix de la fenêtre
                    continuer = 0;
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym)
                    {
                        case SDLK_ESCAPE: // Escape --> quitte le jeu
                            continuer = 0;
                            break;
                        case SDLK_1:
                            jouer(window, renderer);
                            break;
//                        case SDLK_2:
//                            editer(window);
//                            break;
                    }
                    break;
            }

            // On efface l'écran
            SDL_RenderClear(renderer);
            // On affiche l'image du menu
            SDL_RenderCopy(renderer, textureMenu, NULL, &menu);
            // On met à jour l'affichage
            SDL_RenderPresent(renderer);
        }



    SDL_DestroyTexture(textureMenu);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();


    return EXIT_SUCCESS;
}