/*
jeu.c
------

Rôle : Charger les images de Mario selon la direction.
*/


#include "../header/headerInclusion.h"



SDL_Texture *afficherMario(SDL_Renderer *renderer, SDL_Keycode toucheClavier) {
    SDL_Surface *imageMarioHaut = NULL, *imageMarioBas = NULL, *imageMarioDroite = NULL, *imageMarioGauche = NULL;
    SDL_Texture *textureMarioHaut = NULL, *textureMarioBas = NULL, *textureMarioDroite = NULL, *textureMarioGauche = NULL;

//    On instancie les différentes surfaces des positions de Mario
    imageMarioHaut = IMG_Load("../images/mario_haut.gif");
    if (!imageMarioHaut) {
        printf("IMG_Load: %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }
    imageMarioBas = IMG_Load("../images/mario_bas.gif");
    if (!imageMarioBas) {
        printf("IMG_Load: %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }
    imageMarioDroite = IMG_Load("../images/mario_droite.gif");
    if (!imageMarioDroite) {
        printf("IMG_Load: %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }
    imageMarioGauche = IMG_Load("../images/mario_gauche.gif");
    if (!imageMarioGauche) {
        printf("IMG_Load: %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }

//    On instancie les textures des différentes positions de Mario
    textureMarioHaut = SDL_CreateTextureFromSurface(renderer, imageMarioHaut);
    if (textureMarioHaut == NULL) {
        printf("SDL_Texture: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    textureMarioBas = SDL_CreateTextureFromSurface(renderer, imageMarioBas);
    if (textureMarioBas == NULL) {
        printf("SDL_Texture: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    textureMarioDroite = SDL_CreateTextureFromSurface(renderer, imageMarioDroite);
    if (textureMarioDroite == NULL) {
        printf("SDL_Texture: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    textureMarioGauche = SDL_CreateTextureFromSurface(renderer, imageMarioGauche);
    if (textureMarioGauche == NULL) {
        printf("SDL_Texture: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

//    On détruit toutes les surfaces de Mario pour libérer de l'espace
    SDL_FreeSurface(imageMarioHaut);
    SDL_FreeSurface(imageMarioBas);
    SDL_FreeSurface(imageMarioDroite);
    SDL_FreeSurface(imageMarioGauche);

    switch (toucheClavier) {
        case SDLK_UP:
            return textureMarioHaut;
        case SDLK_DOWN:
            return textureMarioBas;
        case SDLK_RIGHT:
            return textureMarioDroite;
        case SDLK_LEFT:
            return textureMarioGauche;
    }

//    On détruit toutes les textures de Mario
    SDL_DestroyTexture(textureMarioHaut);
    SDL_DestroyTexture(textureMarioBas);
    SDL_DestroyTexture(textureMarioDroite);
    SDL_DestroyTexture(textureMarioGauche);

}

