/*
deplacerJoueurOuCaisse.c
------

Rôle : Définition de la hitbox et déplacement du joueur et des caisses.
*/


#include "../header/headerInclusion.h"

//int boxStuck(int carte[TAILLE_BLOCS][TAILLE_BLOCS], SDL_Rect *posMario, int *objectifRestants){
//    if ( (carte[(posMario->y / TAILLE_BLOCS) - 2][posMario->x / TAILLE_BLOCS] == MUR &&
//            carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS)-2] == MUR) ||
//         (carte[(posMario->y / TAILLE_BLOCS) - 2][posMario->x / TAILLE_BLOCS] == MUR &&
//            carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS)+2] == MUR) ||
//         (carte[(posMario->y / TAILLE_BLOCS) + 2][posMario->x / TAILLE_BLOCS] == MUR &&
//             carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS)+2] == MUR) ||
//         (carte[(posMario->y / TAILLE_BLOCS) + 2][posMario->x / TAILLE_BLOCS] == MUR &&
//          carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS)-2] == MUR)
//    )
//    {
//
//    }
//}


void deplacerJoueurOuCaisse(int carte[TAILLE_BLOCS][TAILLE_BLOCS], SDL_Rect *posMario, SDL_Keycode toucheClavier,
                            SDL_Rect *positionCaisse, int *objectifRestants) {
    switch (toucheClavier) {

        case SDLK_UP:
            // Si devant Mario il y a une caisse
            if (carte[(posMario->y / TAILLE_BLOCS) - 1][posMario->x / TAILLE_BLOCS] == CAISSE ||
                    carte[(posMario->y / TAILLE_BLOCS) - 1][posMario->x / TAILLE_BLOCS] == CAISSE_SUR_OBJECTIF){
                // Si après la CAISSE on ne sort pas de l'écran ET qu'il n'y a pas de MUR
                if ((posMario->y)-TAILLE_BLOCS > 0 &&
                    carte[(posMario->y / TAILLE_BLOCS) - 2][posMario->x / TAILLE_BLOCS] != MUR){

                        // On réactualise la carte
                    // en supprimant la précédente position de la caisse
                    // si avant on avait une CAISSE_SUR_OBJECTIF on remet un OBJECTIF
                    if (carte[(posMario->y / TAILLE_BLOCS) - 1][posMario->x / TAILLE_BLOCS] == CAISSE_SUR_OBJECTIF) {
                        *objectifRestants+=1;
                        carte[(posMario->y / TAILLE_BLOCS) - 1][posMario->x / TAILLE_BLOCS] = OBJECTIF;
                    }
                    else carte[(posMario->y / TAILLE_BLOCS) - 1][posMario->x / TAILLE_BLOCS] = VIDE;
                    // en changeant de place la caisse et son état (sur OBJECTIF ou non)
                    if (carte[(posMario->y / TAILLE_BLOCS) - 2][posMario->x / TAILLE_BLOCS] == OBJECTIF) {
                        *objectifRestants-=1;
                        carte[(posMario->y / TAILLE_BLOCS) - 2][posMario->x / TAILLE_BLOCS] = CAISSE_SUR_OBJECTIF;
                    }
                    else carte[(posMario->y / TAILLE_BLOCS) - 2][posMario->x / TAILLE_BLOCS] = CAISSE;
                    // On actualise la position de Mario
                    posMario->y -= TAILLE_BLOCS;
                    // La position x de la CAISSE vaut la position x de Mario
                    positionCaisse->x = posMario->x;
                    // La position y de la CAISSE vaut la position en y de mario + une case
                    positionCaisse->y = posMario->y - TAILLE_BLOCS;
                    break;

                }
            } else{ //Pas de caisse devant Mario
                // Si une case devant on ne sort pas de l'écran ET qu'il n'y a pas de MUR
                if (posMario->y > 0 && carte[posMario->y / TAILLE_BLOCS - 1][posMario->x / TAILLE_BLOCS] != MUR){
                    posMario->y -= TAILLE_BLOCS;
                    break;
                }
            }
            break;



        case SDLK_DOWN:
            // Si devant Mario il y a une caisse
            if (carte[(posMario->y / TAILLE_BLOCS) + 1][posMario->x / TAILLE_BLOCS] == CAISSE ||
                    carte[(posMario->y / TAILLE_BLOCS) + 1][posMario->x / TAILLE_BLOCS] == CAISSE_SUR_OBJECTIF){
                // Si après la CAISSE on ne sort pas de l'écran ET qu'il n'y a pas de MUR
                if ((posMario->y) < HAUTEUR_FENETRE-(2*TAILLE_BLOCS) &&
                    carte[(posMario->y / TAILLE_BLOCS) + 2][posMario->x / TAILLE_BLOCS] != MUR){

                        // On réactualise la carte
                    // en supprimant la précédente position de la caisse
                    // si avant on avait une CAISSE_SUR_OBJECTIF on remet un OBJECTIF
                    if (carte[(posMario->y / TAILLE_BLOCS) + 1][posMario->x / TAILLE_BLOCS] == CAISSE_SUR_OBJECTIF) {
                        carte[(posMario->y / TAILLE_BLOCS) + 1][posMario->x / TAILLE_BLOCS] = OBJECTIF;
                        *objectifRestants+=1;
                    }
                    else carte[(posMario->y / TAILLE_BLOCS) + 1][posMario->x / TAILLE_BLOCS] = VIDE;
                    // en changeant de place la caisse et son état (sur OBJECTIF ou non)
                    if (carte[(posMario->y / TAILLE_BLOCS) + 2][posMario->x / TAILLE_BLOCS] == OBJECTIF) {
                        carte[(posMario->y / TAILLE_BLOCS) + 2][posMario->x / TAILLE_BLOCS] = CAISSE_SUR_OBJECTIF;
                        *objectifRestants-=1;
                    }
                    else carte[(posMario->y / TAILLE_BLOCS) + 2][posMario->x / TAILLE_BLOCS] = CAISSE;
                    // On actualise la position de Mario
                    posMario->y += TAILLE_BLOCS;
                    // La position x de la CAISSE vaut la position x de Mario
                    positionCaisse->x = posMario->x;
                    // La position y de la CAISSE vaut la position en y de mario + une case
                    positionCaisse->y = posMario->y + TAILLE_BLOCS;
                    break;

                }
            } else{ //Pas de caisse devant Mario
                // Si une case devant on ne sort pas de l'écran ET qu'il n'y a pas de MUR
                if (posMario->y < HAUTEUR_FENETRE-TAILLE_BLOCS &&
                    carte[posMario->y / TAILLE_BLOCS + 1][posMario->x / TAILLE_BLOCS] != MUR){
                    posMario->y += TAILLE_BLOCS;
                    break;
                }
            }
            break;



        case SDLK_LEFT:
            // Si à gauche de Mario il y a une caisse
            if (carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS)-1] == CAISSE ||
                    carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS)-1] == CAISSE_SUR_OBJECTIF){
                // Si après la CAISSE on ne sort pas de l'écran ET qu'il n'y a pas de MUR
                if ((posMario->x)-TAILLE_BLOCS > 0 &&
                    carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS)-2] != MUR){

                        // On réactualise la carte
                    // en supprimant la précédente position de la caisse
                    // si avant on avait une CAISSE_SUR_OBJECTIF on remet un OBJECTIF
                    if (carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS)-1] == CAISSE_SUR_OBJECTIF) {
                        carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS) - 1] = OBJECTIF;
                        *objectifRestants+=1;
                    }
                    else carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS)-1] = VIDE;
                    // en changeant de place la caisse et son état (sur OBJECTIF ou non)
                    if (carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS) - 2] == OBJECTIF) {
                        carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS) - 2] = CAISSE_SUR_OBJECTIF;
                        *objectifRestants-=1;
                    }
                    else carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS) - 2] = CAISSE;

                    // On actualise la position de Mario
                    posMario->x -= TAILLE_BLOCS;
                    // La position x de la CAISSE vaut la position x de Mario
                    positionCaisse->x = posMario->x - TAILLE_BLOCS;
                    // La position y de la CAISSE vaut la position en y de mario + une case
                    positionCaisse->y = posMario->y;
                    break;

                }
            } else{ //Pas de caisse devant Mario
                // Si une case devant on ne sort pas de l'écran ET qu'il n'y a pas de MUR
                if (posMario->x > 0 && carte[posMario->y / TAILLE_BLOCS][(posMario->x / TAILLE_BLOCS)-1] != MUR){
                    posMario->x -= TAILLE_BLOCS;
                    break;
                }
            }
            break;



        case SDLK_RIGHT:
            // Si à gauche de Mario il y a une caisse
            if (carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS)+1] == CAISSE ||
                    carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS)+1] == CAISSE_SUR_OBJECTIF){
                // Si après la CAISSE on ne sort pas de l'écran ET qu'il n'y a pas de MUR
                if ((posMario->x) < LARGEUR_FENETRE-(2*TAILLE_BLOCS) &&
                    carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS)+2] != MUR){

                        // On réactualise la carte
                    // en supprimant la précédente position de la caisse
                    // si avant on avait une CAISSE_SUR_OBJECTIF on remet un OBJECTIF
                    if (carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS) + 1] == CAISSE_SUR_OBJECTIF) {
                        carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS) + 1] = OBJECTIF;
                        objectifRestants+=1;
                    }
                    else carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS) + 1] = VIDE;
                    // en changeant de place la caisse et son état (sur OBJECTIF ou non)
                    if (carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS) + 2] == OBJECTIF) {
                        carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS) + 2] = CAISSE_SUR_OBJECTIF;
                        *objectifRestants-=1;
                    }
                    else carte[(posMario->y / TAILLE_BLOCS)][(posMario->x / TAILLE_BLOCS) + 2] = CAISSE;

                    // On actualise la position de Mario
                    posMario->x += TAILLE_BLOCS;
                    // La position x de la CAISSE vaut la position x de Mario
                    positionCaisse->x = posMario->x + TAILLE_BLOCS;
                    // La position y de la CAISSE vaut la position en y de mario + une case
                    positionCaisse->y = posMario->y;
                    break;

                }
            } else{ //Pas de caisse devant Mario
                // Si une case devant on ne sort pas de l'écran ET qu'il n'y a pas de MUR
                if (posMario->x < LARGEUR_FENETRE-TAILLE_BLOCS &&
                    carte[posMario->y / TAILLE_BLOCS][(posMario->x / TAILLE_BLOCS)+1] != MUR){
                    posMario->x += TAILLE_BLOCS;
                    break;
                }
            }
            break;

    }

}